### A web application using Java, JSPs, Servlets, and JDBC. HTML and JavaScript was used for front-end as well as MySQL for a database.
---------------------------------------------------

## Authors

####Jose Caballero - Lead Programmer
Responsible for majority of the back-end functionality of the web app. Set up the majority of the project such as adding libraries and responsible for making connection to a MySQL database.

+	<mailto:jgcaballero@miners.utep.edu>

####Eric Camacho - Lead Designer
Responsible for majority of the front-end. Managed the database design and creation. Also contributed to back-end functionality to help the lead programmer.

+	<mailto:eicamacho@miners.utep.edu>