<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">All Initiatives</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<!-- /.panel-heading -->
				<div class="panel-body">
					<table width="100%"
						class="table table-striped table-bordered table-hover"
						id="dataTables">
						<thead>
							<tr>
								<th>Initiative Id</th>
								<th>Title</th>
								<th>Category</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Description</th>
								<th>Purpose</th>
								<th>Action</th>
								<th>Delete</th>
							</tr>
						</thead>

						<tbody>

							<c:forEach var="tempInitiative" items="${INITIATIVE_LIST}">

								<c:url var="tempLink" value="InitiativeControllerServlet">
									<c:param name="command" value="READ" />
									<c:param name="initiativeID" value="${tempInitiative.getId()}" />
								</c:url>

								<c:url var="deleteLink" value="InitiativeControllerServlet">
									<c:param name="command" value="DELETE" />
									<c:param name="initiativeID" value="${tempInitiative.getId()}" />
								</c:url>

								<tr>
									<td>${tempInitiative.getId()}</td>
									<td>${tempInitiative.getTitle()}</td>
									<td>${tempInitiative.getCategory()}</td>
									<td>${tempInitiative.getStartDate()}</td>
									<td>${tempInitiative.getEndDate()}</td>
									<td>${tempInitiative.getDescription()}</td>
									<td>${tempInitiative.getPurpose()}</td>
									<td><a href="${tempLink}">Update</a></td>
									<td><a href="${deleteLink}" onclick="if (!(confirm('Are you sure you wanna delete that initiative fam?'))) return false">Delete</a></td>
								</tr>

							</c:forEach>



						</tbody>
					</table>
					<!-- /.table-responsive -->
				</div>

				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->