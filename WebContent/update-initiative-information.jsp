<div class="row">
       <div class="col-md-12">
           <div class="panel panel-primary">
                <div class="panel-heading">Initative Information</div>
                    <div class="panel-body">
					<form action="../../../../ACCESS_TeamSpecter/InitiativeControllerServlet" name="Initiative" onsubmit="return validateForm()" method="get">
                        <input type="hidden" name="command" value="UPDATE">
                        <input type="hidden" name="initiativeID"  value="${THE_INITIATIVE.getId()}">
                        
                        <div class="row">
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label><span class="glyphicon glyphicon-star"></span>Title</label>
                                        <input class="form-control" name="title"  id="Title" value="${THE_INITIATIVE.getTitle()}" >
                                    </div>
                            </div>
                            <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Keyword(s)</label>
                                        <input class="form-control" name="keyword" id="Keyword" placeholder="Keyword">
                                    </div> 
                            </div>
                             <div class="col-md-1" id="plus-Buttom">
                                <button type="button" class="btn btn-success btn-circle btn-sm"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                            <div class="col-md-2">
                                    <div class="form-group">
                                        <label><span class="glyphicon glyphicon-star"></span>Category</label>
                                            <select class="form-control" name="category"> 
                                                <option>Learning Communities</option>
                                                <option>Development Workshops</option>
                                                <option>Mentoring</option>
                                                <option>Student Employment
                                                <option>Student Leadership</option>
                                                <option>First Year Experience</option>
                                                <option>Undergraduate Research & Creative Activity</option>
                                                <option>Internship & Practicum</option>
                                                <option>Study Abroad/Study Away</option>
                                                <option>Community Engagement & Service Learning</option>
                                                <option>Capstone Experience</option>
                                            </select>
                                    </div>
                            </div>
                            <div class="col-md-2">
                                    <div class="form-group">
                                        <label><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-calendar"></span>
                                        Start Date</label>
                                        <input class="form-control" id="startDate" name="startDate" placeholder="MM/DD/YYYY" value="${THE_INITIATIVE.getStartDate()}">
                                    </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label><span class="glyphicon glyphicon-star"></span>Description</label>
                                        <textarea class="form-control" rows="3" name="description">${THE_INITIATIVE.getDescription()}</textarea>        
                                </div>
                             </div>
                             <div class="col-md-2 col-md-offset-3">
                                    <div class="form-group">
                                        <label><span class="glyphicon glyphicon-calendar"></span>
                                        End Date</label>
                                        <input class="form-control" name="endDate" id="endDate" placeholder="MM/DD/YYYY" value="${THE_INITIATIVE.getEndDate()}">
                                    </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                    <div class="form-group">
                                        <label>Purpose</label>
                                        <input class="form-control" name="purpose" id="Title" value="${THE_INITIATIVE.getPurpose()}">
                                    </div>
                            </div>
                             <div class="col-md-1" id="plus-Buttom">
                                <button type="button" class="btn btn-success btn-circle btn-sm"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-2">
                                
                                    <div class="form-group">  
                                        <label>Document Type</label>
                                        <select class="form-control" name="documentType" >
                                            <option>Data</option>
                                            <option>Evaluation Report</option>
                                            <option>Annual Report</option>
                                        </select>
                                    </div>
                               
                            </div>
                            <div class="col-md-4">

                           <!-- TODO need to work on this attachment-->
                                    <div class="form-group">  
                                        <label>Attachment/Link</label>
                                        <select class="form-control" >
                                            <option>Option 1</option>
                                            <option>Option 2</option>
                                            <option>Option 3</option>
                                        </select>
                                        <h5> or </h5>
                                        <input type="file">
                                    </div>
                               
                            </div>
                            <div class="col-md-1" id="plus-Buttom">
                                <button type="button" class="btn btn-success btn-circle btn-sm"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-11">
                               
                                    <div class="form-group">  
                                        <label>Website</label>
                                        <input class="form-control" name="website" id="Website">
                                    </div>
                              
                            </div>
                            <div class="col-md-1" id="plus-Buttom">
                                <button type="button" class="btn btn-success btn-circle btn-sm"><i class="fa fa-plus"></i>
                                </button>
                    	   </div>
                        </div>
                        <div class="row">    
                            <div class="col-md-12">
                                <div class="col-md-2 col-md-offset-10">
                                    <div class="modal-footer" style= "border-top-width: 0px;">
                                        <input type="submit" value="Submit" class="btn btn-primary" />
                                     </div>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>