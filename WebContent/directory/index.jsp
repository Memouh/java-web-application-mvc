<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ACCESS System</title>

    <!-- Bootstrap Core CSS -->
    <link href="../resource/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../resource/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="../resource/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../resource/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../resource/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="../resource/vendor/Custom/css/custom.css" rel="stylesheet">
    <link href="../resource/vendor/datatables/css/select.dataTables.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
         <!-- Navigation -->
        <jsp:include page="../resource/html/navigation.jsp" />
        <!-- /#page-wrapper -->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Welcome to Accelerating, Connecting, and Evaluating Student Success (ACCESS)</h3> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form" id="analysis">
                                <div class="row">
                                    <div class="col-md-2 col-md-offset-2">
                                        <label>Search by College </label>
                                    </div>
                                        <div class="col-md-2 col-md-offset-1">
                                            <select  class="form-control" id="keyword" placeholder="">
                                                <option>Engineer</option>
                                                <option>Education</option>
                                                <option>Science</option>
                                            </select>
                                        </div>
                                    </div>
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-2">
                                        <label>Search by Department</label>
                                    </div>
                                        <div class="col-md-2">
                                            <select  class="form-control" id="keyword" placeholder="">
                                                <option>Computer Science</option>
                                                <option>Electric Engineer</option>
                                                <option>Mechanical Engineer</option>
                                            </select>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-2">
                                        <label>Search by Category </label>
                                    </div>
                                        <div class="col-md-2">
                                            <select  class="form-control" id="keyword" placeholder="">
                                                <option>Category 1</option>
                                                <option>Category 2</option>
                                                <option>Category 3</option>
                                            </select>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-2">
                                        <label>Search by Date </label>
                                    </div>
                                        <div class="col-md-2">
                                            <label><span class="glyphicon glyphicon-calendar"></span>
                                            Start Date</label>
                                        <input class="form-control" id="startDate" placeholder="MM/DD/YYYY">
                                        </div>
                                        <div class="col-md-2">
                                            <label><span class="glyphicon glyphicon-calendar"></span>
                                            End Date</label>
                                        <input class="form-control" id="startDate" placeholder="MM/DD/YYYY">
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-2">
                                        <label>Search by Keyword </label>
                                    </div>
                                        <div class="col-md-2">
                                            <input class="form-control" id="keywordSearch" placeholder="Keyword" >
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                        <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                    <table width="100%" class="table table-striped table-bordered table-hover" id="initiatives_table">
                                                        <thead>
                                                            <tr>
                                                                <th>Initative Title</th>
                                                                <th>Initiative Description</th>
                                                                <th>Category</th>
                                                                <th>Owner</th>
                                                                <th>Creation Date</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="resources">
                                                            <tr class="odd gradeX" id="tableRow">
                                                                <td>Initiative Title 1</td>
                                                                <td>Initative Description 1</td>
                                                                <td>Category</td>
                                                                <td>Owner A</td>
                                                                <td class="center">MM-DD-YYYY</td>
                                                            </tr>
                                                            <tr class="odd gradeX" id="tableRow">
                                                                <td>Initiative Title 2</td>
                                                                <td>Initative Description 2</td>
                                                                <td>Category</td>
                                                                <td>Owner A</td>
                                                                <td class="center">MM-DD-YYYY</td>
                                                            </tr>
                                                            <tr class="odd gradeX" id="tableRow">
                                                                <td>Initiative Title 3</td>
                                                                <td>Initative Description 3</td>
                                                                <td>Category</td>
                                                                <td>Owner A</td>
                                                                <td class="center">MM-DD-YYYY</td>
                                                            </tr>
                                                            <tr class="odd gradeX" id="tableRow">
                                                                <td>Initiative Title 4</td>
                                                                <td>Initative Description 4</td>
                                                                <td>Category</td>
                                                                <td>Owner A</td>
                                                                <td class="center">MM-DD-YYYY</td>
                                                            </tr>
                                                            <tr class="odd gradeX" id="tableRow">
                                                                <td>Initiative Title 5</td>
                                                                <td>Initative Description 5</td>
                                                                <td>Category</td>
                                                                <td>Owner A</td>
                                                                <td class="center">MM-DD-YYYY</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                        <div class="row">
                                            <label>How would you like your document?</label>
                                        </div>
                                            <div class="row">
                                                 <div class="col-md-3">
                                                        <label class="radio-inline">
                                                        <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline1" value="option1" checked=""> Summary Report
                                                        </label><label class="radio-inline">
                                                        <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline1" value="option1" checked=""> Detail Report
                                                        </label>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox"> <i class="fa fa-file-excel-o fa-4" aria-hidden="true"></i> 
                                                    </label>
                                                </div>    
                                                <div class="col-md-1">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox"> <i class="fa fa-file-pdf-o fa-4"></i> 
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                     <button type="button" class="btn btn-default btn-sm btn-download">
                                                        <span class="glyphicon glyphicon-download-alt"></span> Download
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                          
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>

    </div>
	<!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../resource/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../resource/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../resource/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../resource/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../resource/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../resource/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="../resource/vendor/datatables/js/dataTables.select.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="../resource/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#initiatives_table').DataTable({
            select:{
                style: 'multi'
            }
        });
    });
    </script>

</body>

</html>