<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">All projects</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<!-- /.panel-heading -->
				<div class="panel-body">
					<table width="100%"
						class="table table-striped table-bordered table-hover"
						id="dataTables">
						<thead>
							<tr>
								<th>Project Id</th>
								<th>Funded</th>
								<th>Title</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Description</th>
								<th>Objectives</th>
								<th>Goal</th>
								<th>Action</th>
								<th>Delete</th>
							</tr>
						</thead>

						<tbody>

							<c:forEach var="tempProject" items="${PROJECT_LIST}">

								<c:url var="tempLink" value="ProjectControllerServlet">
									<c:param name="command" value="READ" />
									<c:param name="projectID" value="${tempProject.getId()}" />
								</c:url>

								<c:url var="deleteLink" value="ProjectControllerServlet">
									<c:param name="command" value="DELETE" />
									<c:param name="projectID" value="${tempProject.getId()}" />
								</c:url>

								<tr>
									<td>${tempProject.getId()}</td>
									<td>${tempProject.getFunded()}</td>
									<td>${tempProject.getTitle()}</td>
									<td>${tempProject.getStartDate()}</td>
									<td>${tempProject.getEndDate()}</td>
									<td>${tempProject.getDescription()}</td>
									<td>${tempProject.getObjective()}</td>
									<td>${tempProject.getGoal()}</td>
									<td><a href="${tempLink}">Update</a></td>
									<td><a href="${deleteLink}" onclick="if (!(confirm('Are you sure you wanna delete that project fam?'))) return false">Delete</a></td>
								</tr>

							</c:forEach>



						</tbody>
					</table>
					<!-- /.table-responsive -->
				</div>

				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->