<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ACCESS System</title>

    <!-- Bootstrap Core CSS -->
    <link href="../resource/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../resource/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="../resource/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../resource/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../resource/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="../resource/vendor/Custom/css/custom.css" rel="stylesheet">
    <link href="../resource/vendor/datatables/css/select.dataTables.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
         <!-- Navigation -->
             <jsp:include page="../resource/html/navigation.jsp" />
        <!-- /#page-wrapper -->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Welcome to Accelerating, Connecting, and Evaluating Student Success (ACCESS)</h3> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Query</div>
                                    <div class="panel-body" id="Project Information">
                                        <div class="row">
                                            <div class="col-md-1">
                                                 <label>Type</label>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form">
                                                    <div class="form-group">
                                                        <select  class="form-control" id="keyword" placeholder="Type">
                                                            <option>Type 1</option>
                                                            <option>Type 2</option>
                                                            <option>Type 3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Filters</div>
                                    <div class="panel-body" id="Project Information">
                                        <div class="row">
                                            <div class="col-sm-4 ">
                                                 <label>Category</label>
                                                <div class="form">
                                                    <div class="form-group">
                                                        <select  class="form-control" id="keyword" placeholder="Type">
                                                            <option>Category 1</option>
                                                            <option>Category 2</option>
                                                            <option>Category 3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                 <label>Classification</label>
                                                <div class="form">
                                                    <div class="form-group">
                                                        <select  class="form-control" id="keyword" placeholder="Type">
                                                            <option>Classification 1</option>
                                                            <option>Classification 2</option>
                                                            <option>Classification 3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                 <label>Graduates</label>
                                                <div class="form">
                                                    <div class="form-group">
                                                        <label class="radio-inline">
                                                        <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline2" value="option2">Yes
                                                        </label>
                                                        <label class="radio-inline">
                                                        <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline3" value="option3">No
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                 <label>Major Field</label>
                                                <div class="form">
                                                    <div class="form-group">
                                                        <select  class="form-control" id="keyword" placeholder="Type">
                                                            <option>Major Field 1</option>
                                                            <option>Major Field 2</option>
                                                            <option>Major Field 3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                 <label>Department</label>
                                                <div class="form">
                                                    <div class="form-group">
                                                        <select  class="form-control" id="keyword" placeholder="Type">
                                                            <option>Department 1</option>
                                                            <option>Department 2</option>
                                                            <option>Department 3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Restrictions</div>
                                    <div class="panel-body" id="Project Information">
                                        <div class="row">
                                            <div class="col-sm-6 ">
                                                 <label>Time to Degree</label>
                                                <div class="form">
                                                    <div class="form-group">
                                                        <select  class="form-control" id="keyword" placeholder="Type">
                                                            <option> 1 Year</option>
                                                            <option> 2 Years</option>
                                                            <option> 3 Years</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Undergraduates to Graduate Advancement</label>
                                                <div class="form">
                                                    <div class="form-group">
                                                        <label class="radio-inline">
                                                        <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline2" value="option2">Yes
                                                        </label>
                                                        <label class="radio-inline">
                                                        <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline3" value="option3">No
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>  
            </div>
        </div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../resource/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../resource/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../resource/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../resource/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../resource/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../resource/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="../resource/vendor/datatables/js/dataTables.select.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="../resource/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#initiatives_table').DataTable({
            select:{
                style: 'multi'
            }
        });
    });
    </script>

</body>

</html>
