<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">All Activities</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<!-- /.panel-heading -->
				<div class="panel-body">
					<table width="100%"
						class="table table-striped table-bordered table-hover"
						id="dataTables">
						<thead>
							<tr>
								<th>Activity Id</th>
								<th>Title</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Description</th>
								<th>Classification</th>
								<th>Target Audience Desc.</th>
								<th>Action</th>
								<th>Delete</th>
							</tr>
						</thead>

						<tbody>

							<c:forEach var="tempActivity" items="${ACTIVITY_LIST}">

								<c:url var="tempLink" value="ActivityControllerServlet">
									<c:param name="command" value="READ" />
									<c:param name="activityID" value="${tempActivity.getId()}" />
								</c:url>

								<c:url var="deleteLink" value="ActivityControllerServlet">
									<c:param name="command" value="DELETE" />
									<c:param name="activityID" value="${tempActivity.getId()}" />
								</c:url>

								<tr>
									<td>${tempActivity.getId()}</td>
									<td>${tempActivity.getTitle()}</td>
									<td>${tempActivity.getStartDate()}</td>
									<td>${tempActivity.getEndDate()}</td>
									<td>${tempActivity.getActivityDescription()}</td>
									<td>${tempActivity.getClassification()}</td>
									<td>${tempActivity.getAudienceDescription()}</td>
									<td><a href="${tempLink}">Update</a></td>
									<td><a href="${deleteLink}" onclick="if (!(confirm('Are you sure you wanna delete that project fam?'))) return false">Delete</a></td>
								</tr>

							</c:forEach>



						</tbody>
					</table>
					<!-- /.table-responsive -->
				</div>

				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->