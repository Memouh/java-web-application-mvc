<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ACCESS System</title>

    <!-- Bootstrap Core CSS -->
    <link href="resource/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="resource/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="resource/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="resource/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resource/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="resource/vendor/Custom/css/custom.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
             <jsp:include page="resource/html/home_nav.jsp" />
              <div id="page-wrapper">
                <div class="row">
                  <div class="col-lg-12">
                    <h1 class="page-header">Welcome</h1>
                  	<h2>The time on the web server is <%= new java.util.Date() %></h2>
                  	<h3>TEAM SPECTER </h3>

					<h3>
					
					<ol>Lead Programmer - Jose Caballero</ol>
					<ol>Designer - Eric Camacho</ol>
					<ol>V&V - Julio Lopez</ol>
					<ol>Analyst - Orlando Tovar</ol>
					
					</h3>
                                    	
                  </div>
                <!-- /.col-lg-12 -->
                </div>
            <!-- /.row -->
               </div>
        <!-- /#page-wrapper -->
      

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="resource/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resource/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="resource/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="resource/vendor/raphael/raphael.min.js"></script>
    <script src="resource/vendor/morrisjs/morris.min.js"></script>
    <script src="resource/data/morris-data.js"></script>
    <script type="text/javascript" src="resource/vendor/Custom/js/navigationresource.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="resource/dist/js/sb-admin-2.js"></script>

</body>

</html>