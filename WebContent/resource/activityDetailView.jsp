<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ACCESS System</title>

    <!-- Bootstrap Core CSS -->
    <link href="resource/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="resource/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="resource/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="resource/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resource/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="resource/vendor/Custom/css/custom.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
         <!-- Navigation -->
        <jsp:include page="html/navigation.jsp" />
        <!-- /#page-wrapper -->
        <div id="page-wrapper">
        <jsp:include page="html/activity/activityHeader.html"/>
        <jsp:include page="html/activity/activityInformation.html"/>
        <jsp:include page="html/activity/activityCourse.html"/>
        <jsp:include page="html/activity/activityParticipant.html"/>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-1 col-lg-offset-6">
                        <button type="button" class="btn btn-success">Save</button>
                    </div>
                    <div class="col-md-2 closer">
                        <button type="button" class="btn btn-success">Save & Return</button>
                    </div>
                    <div class="col-md-1 ">
                        <button type="button" class="btn btn-danger">Delete</button>
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-secondary">Cancel</button>
                    </div>                   
                </div>
            </div>
        </div>

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="resource/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resource/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="resource/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="resource/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="resource/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="resource/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="resource/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            responsive: true
        });
    });
    </script>

</body>

</html>
