<%@ page import = "java.util.*,mvc.ts.model.jdbc.*, mvc.ts.Java.*,mvc.ts.controller.jdbc.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
%>

<% User currentUser = (User) session.getAttribute("currentUser"); %>



<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/../../ACCESS_TeamSpecter/resource/html/"><h1>ACCESS</h1></a>
            </div>


            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
            <!-- User currentUser, use getters from the User class in order to get attributes out of the User object. If user has not logged in, this will be null,
            need to add error prevention here. -->
                <li class="nameSection"> <h3> <%= currentUser.getFirst_name() + " " + currentUser.getLast_name() %> </h3></li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <!--${pageContext.request.contextPath} is needed in order for the project to find the servlets. Otherwise it returns a 404. -->
                        <li><a href="${pageContext.request.contextPath}/LogoutServlet"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
                
                
                
                

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       <li>
                            <a href="/ACCESS_TeamSpecter/myResources/   "><i class="fa fa-sitemap fa-fw"></i> My Resources </a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="/ACCESS_TeamSpecter/directory"><span class="glyphicon glyphicon-book"></span> Directory </a> 
                        </li>
                        <li>
                            <a href="/ACCESS_TeamSpecter/analysis"><i class="fa fa-edit fa-fw"></i> Analysis </a>
                        </li>
                         <li>
                            <a href="/ACCESS_TeamSpecter/ProjectControllerServlet"><i class="glyphicon glyphicon-book"></i> List Projects </a>
                        </li>
                        <li>
                            <a href="/ACCESS_TeamSpecter/ActivityControllerServlet"><i class="glyphicon glyphicon-book"></i> List Activities </a>
                        </li>
                        <li>
                            <a href="/ACCESS_TeamSpecter/InitiativeControllerServlet"><i class="glyphicon glyphicon-book"></i> List Initiatives </a>
                        </li>
                        
                        </li>
                        <!-- /.nav-second-level -->
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        