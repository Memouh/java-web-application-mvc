<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Project Information</div>
			<div class="panel-body" id="Project Information">
				<div class="row">
					<div class="col-lg-4">
						<div class="form">
							<div class="form-group">
							<!-- No se porqe no quiere con el page context, tuve que especificar el camino yo mismo. Ah de ser porque THIS IS FUCKING JAVA. -->
							<form action="../../../../ACCESS_TeamSpecter/ProjectControllerServlet" method="get">
							
								<input type="hidden" name="command" value="UPDATE">
								<label><span class="glyphicon glyphicon-star"></span>Title</label>
								<input class="form-control" name="title" "id="Title">
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="form">
							<div class="form-group">
								<label><span class="glyphicon glyphicon-star"></span>Keyword(s)</label>
								<input class="form-control" name="keywords" "id="Keywords">
							</div>
						</div>
					</div>
					<div class="col-lg-1" id="plus-Buttom">
						<button type="button" class="btn btn-success btn-circle btn-sm">
							<i class="fa fa-plus"></i>
						</button>
					</div>
					<div class="col-md-2  col-md-offset-1">
						<div class="form">
							<div class="form-group">
								<label><span class="glyphicon glyphicon-star"></span><span
									class="glyphicon glyphicon-calendar"></span> Start Date</label> 
									<input class="form-control" name="startDate" id="startDate" placeholder="YYYY/MM/DD">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label><span class="glyphicon glyphicon-star"></span>Description</label>
							<textarea class="form-control" rows="3" name="description" id="description"></textarea>
						</div>
					</div>
					<div class="col-md-2 col-md-offset-2">
						<div class="form">
							<div class="form-group">
								<label><span class="glyphicon glyphicon-calendar"></span>End Date</label> 
								<input class="form-control" name = "endDate" id="endDate" placeholder="YYYY/MM/DD">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form">
							<div class="form-group">
								<label><span class="glyphicon glyphicon-star"></span>Goal</label>
								<input class="form-control" name="goal" "id="goal">
							</div>
						</div>
					</div>
					<div class="col-lg-1" id="plus-Buttom">
						<button type="button" class="btn btn-success btn-circle btn-sm">
							<i class="fa fa-plus"></i>
						</button>
					</div>
					<div class="col-lg-4">
						<div class="form">
							<div class="form-group">
								<label><span class="glyphicon glyphicon-star"></span>Objectives</label>
								<input class="form-control" name="objective" "id="Objectives">
							</div>
						</div>
					</div>
					<div class="col-lg-1" id="plus-Buttom">
						<button type="button" class="btn btn-success btn-circle btn-sm">
							<i class="fa fa-plus"></i>
						</button>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="form">
							<div class="form-group">
								<label><span class="glyphicon glyphicon-star"></span>Website</label>
								<input class="form-control" name="website" "id="Website">
								<h5>or attach a file</h5>
								<input type="file">
							</div>
						</div>

					</div>
					<div class="col-lg-1" id="plus-Buttom">
						<button type="button" class="btn btn-success btn-circle btn-sm">
							<i class="fa fa-plus"> </i>
						</button>

					</div>
				</div>
				<div></div>
				<div class="row">
					<div class="col-lg-11">
						<div class="form">
							<div class="form-group">
								<label>Document Type</label> <select class="form-control">
									<option>Data</option>
									<option><a href="">Evaluation Report</a></option>
									<option><a href="">Annual Report</a></option>
									<option><a href-"">Other</a></option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-lg-1" id="plus-Buttom">
						<button type="button" class="btn btn-success btn-circle btn-sm">
							<i class="fa fa-plus"></i>
						</button>
					</div>
				</div>

				<div class="modal-footer">
					<input type="submit" value="Submit" class="btn btn-primary" />
				</div>
				</form>
			</div>
		</div>
	</div>
</div>