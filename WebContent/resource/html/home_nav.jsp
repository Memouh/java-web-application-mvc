<%@ page
	import="java.util.*,mvc.ts.model.jdbc.*, mvc.ts.Java.*,mvc.ts.controller.jdbc.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<nav class="navbar navbar-default navbar-static-top" role="navigation"
	style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="../../ACCESS_TeamSpecter/"><h1>ACCESS</h1></a>
	</div>
	<!-- /.navbar-header -->

	<ul class="nav navbar-top-links navbar-right">
		<li class="nameSection"></li>
		<!-- /.dropdown -->
		<li class="dropdown"><a class="dropdown-toggle"
			data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>
				<i class="fa fa-caret-down"></i>
		</a>
			<ul class="dropdown-menu dropdown-user">
				<li>
				<button type="btn" class="btn sign-in"data-toggle="modal" data-target="#myLoginModal">Login</button>
				</li>
	</ul>
	<!-- /.dropdown-user -->
	</li>
	<!-- /.dropdown -->
	</ul>
	<!-- /.navbar-top-links -->


	<!-- /.navbar-static-side -->
</nav>

<!-- Modal -->
<div class="modal fade" id="myLoginModal" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header"
				style="background-color: #337ab7; color: #fff">
				<h4 class="modal-title">LOGIN FAM</h4>
				<small>Please enter your information</small>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
					
					<!--Form action = THE SERVLET YOU WANT TO USE, METHOD = either post or get. -->
						<form action="LoginServlet" method="post">
							<div class="form-group">
								<label for="InputEmail">Email address</label> <input
									type="email" class="form-control" id="InputEmail"
									placeholder="Email" name="email">
							</div>
							<div class="form-group">
								<label for="InputPassword">Password</label> <input
									type="password" class="form-control" id="InputPassword"
									placeholder="Password" name="password">
							</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<input type="submit" value="Login" class="btn btn-primary" />
			</div>
			</form>
		</div>

	</div>
</div>