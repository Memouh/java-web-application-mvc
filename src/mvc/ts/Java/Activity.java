package mvc.ts.Java;

public class Activity {
	private int id;
	private String title;
	private String startDate;
	private String endDate;
	private String activityDescription;
	private String classification;
	private String audienceDescription;


	
	public Activity(int id, String title, String startDate, String endDate,String activityDescription, String classification,  
			String audienceDescription){
			
		this.id = id;
		this.title = title;
		this.startDate = startDate;
		this.endDate = endDate;
		this.activityDescription = activityDescription;
		this.classification = classification;
		this.audienceDescription = audienceDescription;
	}
	
	public Activity(int id, String title, String activityDescription, String startDate, String endDate,  
			String audienceDescription){
			
		this.id = id;
		this.title = title;
		this.activityDescription = activityDescription;
		this.startDate = startDate;
		this.endDate = endDate;
		this.audienceDescription = audienceDescription;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}
	
	public String getClassification(){
		return classification;
	}
	
	public void setClassification(String classification){
		this.classification = classification;
	}

	public String getAudienceDescription() {
		return audienceDescription;
	}

	public void setAudienceDescription(String audienceDescription) {
		this.audienceDescription = audienceDescription;
	}
	
	@Override
	public String toString() {
		return " Activity [id=" + id + ", title=" + title + ", description=" + activityDescription + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", classification=" + classification + ", audience description=" + audienceDescription +"]";
	}


}



