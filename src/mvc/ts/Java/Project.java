package mvc.ts.Java;

import java.util.ArrayList;
import java.util.HashMap;

public class Project {
	private int id;
	private int funded;
	private String title;
	private String startDate;
	private String endDate;
	private String description;
	private String objective;
	private String goal;
	private String keywords;
	private String website;
	private HashMap document;

	public Project(int id, int funded, String title, String description, String startDate, String endDate, String goal,
			String objective, String keywords, String website, HashMap document) {
		this.id = id;
		this.setFunded(funded);
		this.title = title;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.goal = goal;
		this.objective = objective;
		this.keywords = keywords;
		this.website = website;
		this.document = document;
	}

	public Project(int id, int funded, String title, String startDate, String endDate, String description, String objective,
			String goal) {
		this.id = id;
		this.setFunded(funded);
		this.title = title;
		this.startDate = startDate;
		this.endDate = endDate;
		this.description = description;
		this.goal = goal;
		this.objective = objective;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFunded() {
		return funded;
	}

	public void setFunded(int funded) {
		this.funded = funded;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getGoal() {
		return goal;
	}

	public void setGoal(String goal) {
		this.goal = goal;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public HashMap getDocument() {
		return document;
	}

	public void setDocument(HashMap document) {
		this.document = document;
	}

	@Override
	public String toString() {
		return "Project [id=" + id + ", title=" + title + ", description=" + description + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", goal=" + goal + ", objective=" + objective + ", keywords=" + keywords
				+ ", website=" + website + ", document=" + document + "]";
	}

}
