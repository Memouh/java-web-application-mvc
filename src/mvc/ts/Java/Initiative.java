package mvc.ts.Java;

import java.util.ArrayList;
import java.util.HashMap;

import mvc.ts.model.jdbc.InitiativeDBUtil;

public class Initiative {
private int id;
private String title;
private String category;
private String startDate;
private String endDate;
private String description;
private String purpose;
private ArrayList keywords;
private ArrayList website;
private HashMap document;

private InitiativeDBUtil iniDBUtil;



public Initiative(int id, String title, String category, String startDate, String endDate, String description,
		String purpose, ArrayList keywords, ArrayList website, HashMap document) {
	this.id = id;
	this.title = title;
	this.category = category;
	this.startDate = startDate;
	this.endDate = endDate;
	this.description = description;
	this.purpose = purpose;
	this.keywords = keywords;
	this.website = website;
	this.document = document;
}

public Initiative(int id, String title, String category, String startDate, String endDate, String description,
		String purpose) {
	this.id = id;
	this.title = title;
	this.category = category;
	this.startDate = startDate;
	this.endDate = endDate;
	this.description = description;
	this.purpose = purpose;
}


public void create(){
	int id=0;
	try {
		iniDBUtil.create(id,title, category, startDate, endDate, description, purpose);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getCategory() {
	return category;
}
public void setCategory(String category) {
	this.category = category;
}
public String getStartDate() {
	return startDate;
}
public void setStartDate(String startDate) {
	this.startDate = startDate;
}
public String getEndDate() {
	return endDate;
}
public void setEndDate(String endDate) {
	this.endDate = endDate;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getPurpose() {
	return purpose;
}
public void setPurpose(String purpose) {
	this.purpose = purpose;
}
public ArrayList getKeywords() {
	return keywords;
}
public void setKeywords(ArrayList keywords) {
	this.keywords = keywords;
}
public ArrayList getWebsite() {
	return website;
}
public void setWebsite(ArrayList website) {
	this.website = website;
}
public HashMap getDocument() {
	return document;
}
public void setDocument(HashMap document) {
	this.document = document;
}
@Override
public String toString() {
	// TODO Auto-generated method stub
	return super.toString();
}



}
