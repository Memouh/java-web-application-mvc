package mvc.ts.controller.jdbc;

import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mvc.ts.model.jdbc.ActivityDBUtil;

/**
 * Servlet implementation class CreateProjectServlet
 */
@WebServlet("/CreateActivityServlet")
public class CreateActivityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Declaring the dataSource and the classes that will be used in this class.
	 */
	@Resource(name = "jdbc/team-specter")
	private DataSource dataSource;
	private ActivityDBUtil activityDBUtil;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateActivityServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		super.init();

		// Initialize UserDBUtil class with the dataSource so the class knows
		// which DB to use.
		try {
			activityDBUtil = new ActivityDBUtil(dataSource);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// Set up the print writer
		PrintWriter out = response.getWriter();
		response.setContentType("text/plain");

		// Get input parameter from home_nav through the use of the request
		// parameter.
		String title = request.getParameter("title");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String activityDescription = request.getParameter("activityDescription");
		String classification = request.getParameter("classification");
		String audienceDescription = request.getParameter("audienceDescription");
		
//		try {
//			activityDBUtil.getActivity(title, startDate, endDate, activityDescription, classification, audienceDescription);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		try {
			activityDBUtil.create(title, startDate, endDate, activityDescription, classification, audienceDescription);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		out.println("CREATE SERVLET REACHED");
   //     out.println(isFunded + " was the funded status entered.");
		out.println(title + " was the title entered.");
		out.println(activityDescription + " was the description entered");
		out.println(startDate + " was the start date entered");
		out.println(endDate + " was the end date entered");
		out.println(classification + " was the classification entered");
		out.println(audienceDescription + " was the audience description entered");
	}

}