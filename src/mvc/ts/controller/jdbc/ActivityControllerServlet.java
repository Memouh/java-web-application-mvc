package mvc.ts.controller.jdbc;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mvc.ts.Java.Activity;
import mvc.ts.Java.Project;
import mvc.ts.model.jdbc.ActivityDBUtil;
import mvc.ts.model.jdbc.ProjectDBUtil;

/**
 * Servlet implementation class ActivityControllerServlet
 */
@WebServlet("/ActivityControllerServlet")
public class ActivityControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(name = "jdbc/team-specter")
	private DataSource dataSource;
	private ActivityDBUtil activityDBUtil;

	@Override
	public void init() throws ServletException {
		super.init();

		// Initialize UserDBUtil class with the dataSource so the class knows
		// which DB to use.
		try {
			activityDBUtil = new ActivityDBUtil(dataSource);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			String command = request.getParameter("command");

			if (command == null) {
				command = "LIST";
			}

			switch (command) {

			case "LIST":
				getActivities(request, response);
				break;

			case "CREATE":
				createActivity(request, response);
				break;

			case "READ":
				readActivity(request, response);
				break;

			case "UPDATE":
				updateActivity(request, response);
				break;

			case "DELETE":
				deleteProject(request, response);
				break;
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	private void createActivity(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String title = request.getParameter("title");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String description = request.getParameter("activityDescription");
		String classification = request.getParameter("classification");
		String audienceDescription = request.getParameter("audienceDescription");

		activityDBUtil.createActivity(title, startDate, endDate, description, classification, audienceDescription);

		getActivities(request, response);

	}

	private void readActivity(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String activityID = request.getParameter("activityID");

		Activity theActivity = activityDBUtil.getActivity(activityID);

		request.setAttribute("THE_ACTIVITY", theActivity);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/update-activity-information.jsp");
		dispatcher.forward(request, response);

	}
	
	private void updateActivity(HttpServletRequest request, HttpServletResponse response) throws Exception{

		int id = Integer.parseInt(request.getParameter("activityID"));
		
		String title = request.getParameter("title");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String description = request.getParameter("activityDescription");
		String classification = request.getParameter("classification");
		String audienceDescription = request.getParameter("audienceDescription");
		
		Activity updatedActivity = new Activity(id, title, startDate, endDate, description, classification, audienceDescription);
		
		activityDBUtil.updateActivity(updatedActivity);
		
		getActivities(request,response);
		
	}

	private void deleteProject(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String activityID = request.getParameter("activityID");

		activityDBUtil.deleteActivity(activityID);

		getActivities(request, response);

	}

	private void getActivities(HttpServletRequest request, HttpServletResponse response) throws Exception {

		List<Activity> activities = activityDBUtil.getActivities();

		request.setAttribute("ACTIVITY_LIST", activities);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/list-activities.jsp");
		dispatcher.forward(request, response);

	}

}
