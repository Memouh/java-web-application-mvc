package mvc.ts.controller.jdbc;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.ts.jsp.FunUtils;

import mvc.ts.Java.Initiative;
import mvc.ts.Java.Project;
import mvc.ts.Java.User;
import mvc.ts.model.jdbc.InitiativeDBUtil;

/**
 * Servlet implementation class CreateInitiativeServlet
 */
@WebServlet("/InitiativeControllerServlet")
public class InitiativeControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name = "jdbc/team-specter")
	private DataSource dataSource;
    private InitiativeDBUtil iniDBUtil;
	
    public InitiativeControllerServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		
		try {
			iniDBUtil = new InitiativeDBUtil(dataSource);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// Set up the print writer
//				
//				// Get input parameter from home_nav through the use of the request
//				// parameter.
//				String command = request.getParameter("command");
//				if(command==null){
//					command="List";
//				}
//				switch(command){
//				case "List":
//					break;
//				case "CREATE":
//					createInitiative(request,response);
//				}
//	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
			String command = request.getParameter("command");
			
			if(command == null){
				command = "LIST";
			}
			
			switch(command){
			
			case "LIST":
				getInitiatives(request,response);
				break;
				
			case "CREATE":
				createInitiative2(request,response);
				break;
				
			case "READ":
				readInitiative(request, response);
				break;
				
			case "UPDATE":
				updateInitiative(request, response);
				break;
				
			case "DELETE":
				deleteInitiative(request, response);
				break;
			}
						
			} catch (Exception e) {
			throw new ServletException(e);
		}	
	}
	

	private void getInitiatives(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		List<Initiative> initiatives = iniDBUtil.getInitiatives();

		request.setAttribute("INITIATIVE_LIST", initiatives);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/list-initiatives.jsp");
		dispatcher.forward(request, response);
		
	}

	private void createInitiative(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
				PrintWriter out = response.getWriter();
				response.setContentType("text/plain");
				boolean appropriateDates = false;
				System.out.println("method call to create Initiative");
				String title = request.getParameter("title");
				String startDate = request.getParameter("startDate");
				String description = request.getParameter("description");
				String category= request.getParameter("category");
				String endDate=request.getParameter("endDate");
				String purpose=request.getParameter("purpose");
				
				
				HttpSession session = request.getSession();
				User currentUser = (User) session.getAttribute("currentUser"); 
				
			try{ endDate=FunUtils.makeStringToDate(endDate); }
			catch (Exception e) { endDate=null; }
 					
 
			try {
				appropriateDates = FunUtils.startDateBeforeEndDate( FunUtils.makeStringToDate(startDate),endDate);
			} 
			catch (ParseException e1) { appropriateDates=false; }
			
			if(appropriateDates&& !iniDBUtil.alreadyExist(title, startDate, category, description)){
				try {
					iniDBUtil.create(currentUser.getId(),title, category, FunUtils.makeStringToDate(startDate), endDate, description, purpose);

					out.println("Has been stored to the DB");

				} catch (Exception e) {
					e.printStackTrace();
					}
				}
			  else 
				out.println("Has not been stored to the DB");
		
	}
	
	private void createInitiative2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String title = request.getParameter("title");
		String category= request.getParameter("category");
		String startDate = request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String description = request.getParameter("description");	
		String purpose=request.getParameter("purpose");
	
		iniDBUtil.createInitiative(title, category,  startDate, endDate, description, purpose);
					
		getInitiatives(request, response);
		
	}
	
	private void readInitiative(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String initiativeID = request.getParameter("initiativeID");
		
		Initiative theInitiative = iniDBUtil.getInitiative(initiativeID);
		
		request.setAttribute("THE_INITIATIVE" , theInitiative);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/update-initiative-information.jsp");
		dispatcher.forward(request, response);
		
		
	}

	private void updateInitiative(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		int id = Integer.parseInt(request.getParameter("initiativeID"));
		
		String title = request.getParameter("title");
		String category= request.getParameter("category");
		String startDate = request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String description = request.getParameter("description");	
		String purpose=request.getParameter("purpose");
		
		Initiative updatedInitiative = new Initiative(id, title, category, startDate, endDate, description, purpose);
		
		iniDBUtil.updateProject(updatedInitiative);
		
		getInitiatives(request,response);
		
	}
	
	private void deleteInitiative(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String initiativeID = request.getParameter("initiativeID");
		
		iniDBUtil.deleteInitiative(initiativeID);
		
		getInitiatives(request,response);
		
	}
	
}
