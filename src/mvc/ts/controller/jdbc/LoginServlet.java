package mvc.ts.controller.jdbc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import mvc.ts.Java.User;
import mvc.ts.model.jdbc.UserDBUtil;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(name = "jdbc/team-specter")
	private DataSource dataSource;
	private UserDBUtil dbUtil;

	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		super.init();

		try {
			dbUtil = new UserDBUtil(dataSource);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		// request.getRequestDispatcher("resource/html/navigation.jsp").include(request,
		// response);

		String email = request.getParameter("email");
		String password = request.getParameter("password");

		try {
			User user = dbUtil.login(email, password);

			if (user != null) {
				HttpSession session = request.getSession();
				session.setAttribute("currentUser", user);
				response.sendRedirect("resource/html/");
				
//				out.print("Welcome fam, "+ session.getAttribute("currentUser"));
//				
//				User user123 = (User) session.getAttribute("currentUser");
//				
//				out.print(user123.getFirst_name());

				
				
			} else {
				 request.getRequestDispatcher("index.jsp").include(request,response);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
