package mvc.ts.controller.jdbc;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mvc.ts.Java.Project;
import mvc.ts.model.jdbc.ProjectDBUtil;

/**
 * Servlet implementation class ProjectControllerServlet
 */
@WebServlet("/ProjectControllerServlet")
public class ProjectControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name = "jdbc/team-specter")
	private DataSource dataSource;
	private ProjectDBUtil proDBUtil;

	@Override
	public void init() throws ServletException {
		super.init();

		// Initialize UserDBUtil class with the dataSource so the class knows
		// which DB to use.
		try {
			proDBUtil = new ProjectDBUtil(dataSource);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
			String command = request.getParameter("command");
			
			if(command == null){
				command = "LIST";
			}
			
			switch(command){
			
			case "LIST":
				getProjects(request,response);
				break;
				
			case "CREATE":
				createProject(request,response);
				break;
				
			case "READ":
				readProject(request, response);
				break;
				
			case "UPDATE":
				updateProject(request, response);
				break;
				
			case "DELETE":
				deleteProject(request, response);
				break;
			}
						
			} catch (Exception e) {
			throw new ServletException(e);
		}	
	}
	
	private void createProject(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String title = request.getParameter("title");
		String description = request.getParameter("description");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String goal = request.getParameter("goal");
		String objective = request.getParameter("objective");
	
		proDBUtil.create(0, title, startDate, endDate, description, objective, goal);
					
		getProjects(request, response);
		
	}

	private void readProject(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String projectID = request.getParameter("projectID");
		
		Project theProject = proDBUtil.getProject(projectID);
		
		request.setAttribute("THE_PROJECT" , theProject);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/update-project-information.jsp");
		dispatcher.forward(request, response);
	}
	
	private void updateProject(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		int id = Integer.parseInt(request.getParameter("projectID"));
		
		String title = request.getParameter("title");
		String description = request.getParameter("description");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String goal = request.getParameter("goal");
		String objective = request.getParameter("objective");
		
		Project updatedProject = new Project(id, 0, title, startDate, endDate, description, objective, goal);
		
		proDBUtil.updateProject(updatedProject);
		
		getProjects(request,response);
		
		
	}

	private void deleteProject(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String projectID = request.getParameter("projectID");
		
		proDBUtil.deleteProject(projectID);
		
		getProjects(request,response);
		
	}
	
	private void getProjects(HttpServletRequest request, HttpServletResponse response) throws Exception  {

		List<Project> projects = proDBUtil.getProjects();

		request.setAttribute("PROJECT_LIST", projects);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/list-projects.jsp");
		dispatcher.forward(request, response);

	}

}
