package mvc.ts.controller.jdbc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mvc.ts.Java.User;
import mvc.ts.model.jdbc.UserDBUtil;

/**
 * Servlet implementation class UserControllerServlet
 */
@WebServlet("/UserControllerServlet")
public class UserControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/team-specter")
	private DataSource dataSource;
	private UserDBUtil dbUtil;
	//private User user;
	
	

		
	
	@Override
	public void init() throws ServletException {
		super.init();
		
		try{
			dbUtil = new UserDBUtil(dataSource);
		}
		catch(Exception e){
			throw new ServletException(e);
		}
	}





	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		response.setContentType("text/html");
//		
//		PrintWriter out = response.getWriter();
//		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		try {
			verify_login(request, response, email, password);
			
			
			
		} catch (Exception e) {
			throw new ServletException(e);
		}

//		out.println("!" + email + password + " were entered");
		
	}
	
	private boolean verify_login(HttpServletRequest request, HttpServletResponse response, String email, String pass) throws Exception {
		//List<User> user = dbUtil.login(email, pass);
		
		User user = dbUtil.login(email, pass);
		
		if (user != null){
			request.setAttribute("USER_INFO", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("list-users.jsp");
			dispatcher.forward(request, response);
		} else {
			
			return false;
		}
			
		
		return false;

	}





	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			listUsers(request, response);
			} catch (Exception e) {
			throw new ServletException(e);
		}	
	}


	



	private void listUsers(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<User> users = dbUtil.listUsers();
		
		request.setAttribute("USER_LIST", users);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/list-users.jsp");
		dispatcher.forward(request, response);
		
		
	}

}
