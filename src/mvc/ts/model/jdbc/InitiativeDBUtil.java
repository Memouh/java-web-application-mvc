package mvc.ts.model.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import com.ts.jsp.FunUtils;

import mvc.ts.Java.Initiative;
import mvc.ts.Java.Project;



public class InitiativeDBUtil {
	
	@Resource(name = "jdbc/team-specter")
	private DataSource dataSource;	
	
	public InitiativeDBUtil(){
	}
	
	public InitiativeDBUtil (DataSource theDataSource) throws Exception{
		this.dataSource=theDataSource;
	}
	
	public void create(int uid, String title, String category, String startDate,
		String endDate, String description, String purpose) throws Exception{
		java.sql.Connection myConn = null;
		java.sql.Statement st = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		// the mysql insert statement
	    String queryInitiative = " insert into Initiative (Title, Category, Start, End, Description, Purpose)"
	        + " values (?, ?, ?, ?, ?, ?)";
	    String queryAdministrative= "INSERT INTO `Initiative_Administratives`(AdministrativeID_InitiativeFK,IAID_UserFK,Role) VALUES (?,?,?)";
	    //INSERT INTO `team-specter`.`initiative` (`Title`, `Category`, `Start`, `End`, `Description`, `Purpose`) VALUES ('title', 'Mentoring', '2016-12-09', '2016-12-10', 'desc', 'purpose');
	    
		try{ 
		//get connection
			myConn = dataSource.getConnection();
			
		 // create the mysql insert preparedstatement
		    pst = myConn.prepareStatement(queryInitiative);
		    pst.setString(1, title);
			pst.setString(2, category);
			pst.setString(3, startDate);
			String endValue=null;
			if(endDate !=null){
				endValue=endDate;
				pst.setString(4, endDate);
				}
			else
				pst.setString(4, null);
			
			pst.setString(5, description);
			pst.setString(6, purpose);

		//execute query
			pst.execute();
			int initId =getInitiativeID(title,category,startDate,endValue,description,purpose);
			pst=myConn.prepareStatement(queryAdministrative);
			pst.setString(1,Integer.toString(initId));
			pst.setString(2, Integer.toString(uid));
			pst.setString(3, "Owner");
			pst.execute();
		} finally {
			close(myConn, st, rs);
		}
	}
	
	public void createInitiative(String title, String category, String startDate, String endDate, String description,
			String purpose) throws Exception {
		
		Connection myConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String sql = "insert into initiative (Title, Category,  Start, End, Description, Purpose)"
				+ " values (?, ?, ?, ?, ?, ?)";
		
		try{
			
			myConn = dataSource.getConnection();
			
			// Create the mySQL insert prepared statement
			pst = myConn.prepareStatement(sql);
			
			pst.setString(1, title);
			pst.setString(2, category);
			pst.setString(3, FunUtils.makeStringToDate(startDate));
			String endDateValue;
			
			try {
				endDateValue = FunUtils.makeStringToDate(endDate);
				pst.setString(4, endDateValue);
			} catch (Exception e) {
				endDateValue = "0000-00-00";
				pst.setString(4, endDateValue);
			}

			pst.setString(5, description);
			pst.setString(6, purpose);

			// execute query
			pst.execute();
			
		} finally {
			close(myConn, pst, rs);
		}		
	}
	
	
	public boolean alreadyExist(String title, String startDate, String category, String description){
		Connection myConn = null;
		Statement st = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String initiativeID="select ID from Initiative where Title= ? and Category=? and StartDate=? and Description=?;";
		try{
			myConn=dataSource.getConnection();
		    pst = myConn.prepareStatement(initiativeID); //create the mysql statement
		    pst.setString(1,title);
		    pst.setString(2, startDate);
		    pst.setString(3, category);
		    pst.setString(4,description);
			rs=pst.executeQuery(); //Execute the query
		} 
		catch (SQLException e) { 
			close(myConn, st, rs);
			return false; 
		}
		close(myConn, st, rs);
		return true;
	}
	
	public int getInitiativeID(String title, String category, String startDate, String endDate, String description, String purpose)throws Exception{
		int idIni=5;
		java.sql.Connection myConn = null;
		java.sql.Statement st = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try{ 
			//get connection
				myConn = dataSource.getConnection();
				
				// the mysql insert statement
				String retrieveNewInitiative="select ID from Initiative where Title= ? and Category=? and StartDate=?"
					    +"and EndDate=? and Description=? and Purpose=?;";

			 // create the mysql select preparedstatement
			    pst = myConn.prepareStatement(retrieveNewInitiative);
			    pst.setString(1,title);
				pst.setString(2,category);
				pst.setString(3,startDate);
				pst.setString(4, endDate);
				pst.setString(5, description);
				pst.setString(6, purpose);
				
			//execute query
				rs=pst.executeQuery();
				System.out.println(pst.toString());
				System.out.println(rs.toString());
				 while (rs.next()) {
					   idIni = rs.getInt("ID");
					   return idIni;
				 }
			} 
		finally {
				close(myConn, st, rs);
			}
		return idIni;
	}
	
	
	private void close(Connection myConn, java.sql.Statement st, ResultSet rs){
		try{
			if(rs != null){
				rs.close();
			}
			if(st != null){
				st.close();
			}
			if(myConn != null){
				myConn.close();
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

	public List<Initiative> getInitiatives() throws Exception {
		
		List<Initiative> initiatives = new ArrayList<>();

		Connection myConn = null;
		Statement st = null;
		ResultSet rs = null;
		
		try{
			
			myConn = dataSource.getConnection();

			String sql = "select * from initiative";
			
			st = myConn.createStatement();
			rs = st.executeQuery(sql);
			
			// Process the fucking result set
						while (rs.next()) {
							int id = rs.getInt("ID");
							String Title = rs.getString("Title");
							String Category = rs.getString("Category");
							String Start = rs.getString("Start");
							String End = rs.getString("End");
							String Description = rs.getString("Description");
							String Purpose = rs.getString("Purpose");

							Initiative tempInitiative = new Initiative(id, Title, Category, Start, End, Description, Purpose);

							initiatives.add(tempInitiative);
						}			
			return initiatives;
		} finally {
			
		}
		
	}

	public void updateProject(Initiative updatedInitiative) throws Exception {

		Connection myConn = null;
		PreparedStatement pst = null;

		try {

			myConn = dataSource.getConnection();

			String sql = "UPDATE initiative "
					+ " set Title=?, Category=?, Start=?, End=?, Description=?, Purpose=? WHERE ID=?";

			pst = myConn.prepareStatement(sql);

			pst.setString(1, updatedInitiative.getTitle());
			pst.setString(2, updatedInitiative.getCategory());
			pst.setString(3, updatedInitiative.getStartDate());
			pst.setString(4, updatedInitiative.getEndDate());
			pst.setString(5, updatedInitiative.getDescription());
			pst.setString(6, updatedInitiative.getPurpose());
			pst.setInt(7, updatedInitiative.getId());

			pst.execute();
		} finally {
			close(myConn, pst, null);
		}

	}

	public Initiative getInitiative(String initiativeID) throws Exception {

		Initiative theInitiative = null;

		Connection myConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int theInitiativeID;
		
		try{
			
			theInitiativeID = Integer.parseInt(initiativeID);

			myConn = dataSource.getConnection();

			String sql = "select * from initiative WHERE ID = ?";

			pst = myConn.prepareStatement(sql);

			pst.setInt(1, theInitiativeID);

			rs = pst.executeQuery();
			if (rs.next()) {
				String Title = rs.getString("Title");
				String Category = rs.getString("Category");
				String Start = rs.getString("Start");
				String End = rs.getString("End");
				String Description = rs.getString("Description");
				String Purpose = rs.getString("Purpose");

				theInitiative = new Initiative(theInitiativeID, Title, Category,  Start, End, Description, Purpose);
			} else {
				throw new Exception("Could not find that project id: " + theInitiativeID);
			}
			return theInitiative;
		} finally {
			close(myConn, pst, rs);
		}
		
	}

	public void deleteInitiative(String initiativeID) throws Exception{

		Connection myConn = null;
		PreparedStatement pst = null;
		
		try{
			
			int inijID = Integer.parseInt(initiativeID);
			
			myConn = dataSource.getConnection();
			
			String sql = "DELETE from initiative WHERE ID=?";
			
			pst = myConn.prepareStatement(sql);
			
			pst.setInt(1, inijID);
			
			pst.execute();
			
		} finally {
			close(myConn, pst, null);			
		}
		
	}

	
	
}
