package mvc.ts.model.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import com.ts.jsp.FunUtils;

import mvc.ts.Java.Project;
import mvc.ts.Java.User;

public class ProjectDBUtil {

	@Resource(name = "jdbc/team-specter")
	private DataSource dataSource;

	public ProjectDBUtil() {
	}

	public ProjectDBUtil(DataSource theDataSource) {
		this.dataSource = theDataSource;
	}

	public void create(int funded, String title, String startDate, String endDate, String description,
			String objectives, String goal) throws Exception {
		int isFunded = 0;
		java.sql.Connection myConn = null;
		java.sql.Statement st = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		// the mySQL Insert Statement
		String queryProject = "insert into Project (Funded, Title, Start, End, Description, Objectives, Goal)"
				+ " values (?, ?, ?, ?, ?, ?, ?)";
		// String queryAdministrative = "INSERT INTO `Project_Administrative`
		// (AdministrativeID_ProjectFK, PAID_UserFK,Role) VALUES(?,?,?)";

		try {
			// get connection
			myConn = dataSource.getConnection();

			// Create the mySQL insert prepared statement
			pst = myConn.prepareStatement(queryProject);
			pst.setInt(1, isFunded);
			pst.setString(2, title);
			pst.setString(3, FunUtils.makeStringToDate(startDate));
			String endDateValue;

			try {
				endDateValue = FunUtils.makeStringToDate(endDate);
				pst.setString(4, endDateValue);
			} catch (Exception e) {
				endDateValue = "0000-00-00";
				pst.setString(4, endDateValue);
			}

			pst.setString(5, description);
			pst.setString(6, objectives);
			pst.setString(7, goal);

			// execute query
			pst.execute();
			/*
			 * int initID = getProject(title,
			 * FunUtils.makeStringToDate(startDate), endDateValue, description,
			 * objectives, goal); // pst =
			 * myConn.prepareStatement(queryAdministrative);
			 * pst.setString(1,Integer.toString(initID)); // pst.setString(2,
			 * Integer.toString(uid)); pst.setString(3, "Owner"); //
			 * pst.execute();
			 */
		} finally {
			close(myConn, st, rs);
		}
	}

	public List<Project> getProjects() throws Exception {

		List<Project> projects = new ArrayList<>();

		Connection myConn = null;
		Statement st = null;
		ResultSet rs = null;

		try {
			// Get a connection fams
			myConn = dataSource.getConnection();

			// Create the mothafucking sql statement
			String sql = "select * from project";

			// Execute the fucking query
			st = myConn.createStatement();
			rs = st.executeQuery(sql);

			// Process the fucking result set
			while (rs.next()) {
				int id = rs.getInt("ID");
				int Funded = rs.getInt("Funded");
				String Title = rs.getString("Title");
				String Start = rs.getString("Start");
				String End = rs.getString("End");
				String Description = rs.getString("Description");
				String Objectives = rs.getString("Objectives");
				String Goal = rs.getString("Goal");

				Project tempProject = new Project(id, Funded, Title, Start, End, Description, Objectives, Goal);

				projects.add(tempProject);
			}
			return projects;
		} finally {
			close(myConn, st, rs);
		}
	}

	public int getProject(int funded, String title, String startDate, String endDate, String description,
			String objectives, String goal) throws Exception {
		int idIni = 5;
		int isFunded = 0;
		java.sql.Connection myConn = null;
		java.sql.Statement st = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			// get connection
			myConn = dataSource.getConnection();

			// mySQL Insert Statement
			String retrieveNewProject = "selectID from Project where Title = ? and StartDate = ?"
					+ " and EndDate = ? and Description = ? and Objectives = ? and Goal = ?;";
			// create mySQL insert prepared statement
			pst = myConn.prepareStatement(retrieveNewProject);
			pst.setInt(1, isFunded);
			pst.setString(1, title);
			pst.setString(2, startDate);
			pst.setString(3, endDate);
			pst.setString(4, description);
			pst.setString(5, objectives);
			pst.setString(6, goal);

			// execute query
			rs = pst.executeQuery();
			System.out.println(pst.toString());
			System.out.println(rs.toString());
			while (rs.next()) {
				idIni = rs.getInt("ID");
				return idIni;
			}
		} finally {
			close(myConn, st, rs);
		}
		return idIni;
	}

	private void close(Connection myConn, java.sql.Statement st, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
			if (myConn != null) {
				myConn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Project getProject(String projectID) throws Exception {

		Project theProject = null;

		Connection myConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int theProjectID;

		try {
			theProjectID = Integer.parseInt(projectID);

			myConn = dataSource.getConnection();

			String sql = "select * from project WHERE ID = ?";

			pst = myConn.prepareStatement(sql);

			pst.setInt(1, theProjectID);

			rs = pst.executeQuery();

			if (rs.next()) {
				int Funded = rs.getInt("Funded");
				String Title = rs.getString("Title");
				String Start = rs.getString("Start");
				String End = rs.getString("End");
				String Description = rs.getString("Description");
				String Objectives = rs.getString("Objectives");
				String Goal = rs.getString("Goal");

				theProject = new Project(theProjectID, Funded, Title, Start, End, Description, Objectives, Goal);
			} else {
				throw new Exception("Could not find that project id: " + theProjectID);
			}

			return theProject;
		} finally {
			close(myConn, pst, rs);
		}
	}

	public void updateProject(Project updatedProject) throws Exception {

		Connection myConn = null;
		PreparedStatement pst = null;

		try {

			myConn = dataSource.getConnection();

			String sql = "UPDATE project "
					+ " set Title=?, Start=?, End=?, Description=?, Objectives=?, Goal=? WHERE ID=?";

			pst = myConn.prepareStatement(sql);

			pst.setString(1, updatedProject.getTitle());
			pst.setString(2, updatedProject.getStartDate());
			pst.setString(3, updatedProject.getEndDate());
			pst.setString(4, updatedProject.getDescription());
			pst.setString(5, updatedProject.getObjective());
			pst.setString(6, updatedProject.getGoal());
			pst.setInt(7, updatedProject.getId());

			pst.execute();
		} finally {
			close(myConn, pst, null);
		}

	}

	public void deleteProject(String projectID) throws Exception {
		
		Connection myConn = null;
		PreparedStatement pst = null;
		
		try{
			
			int projID = Integer.parseInt(projectID);
			
			myConn = dataSource.getConnection();
			
			String sql = "DELETE from project WHERE ID=?";
			
			pst = myConn.prepareStatement(sql);
			
			pst.setInt(1, projID);
			
			pst.execute();
			
		} finally {
			close(myConn, pst, null);			
		}
				
	}

}
