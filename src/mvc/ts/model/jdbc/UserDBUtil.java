package mvc.ts.model.jdbc;

import mvc.ts.Java.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.mysql.jdbc.Statement;


public class UserDBUtil {
	
	private DataSource dataSource;
	//public static int i;
	
	public UserDBUtil (DataSource theDataSource){
		dataSource = theDataSource;
	}
	
	public User login(String inputEmail, String inputPassword) throws Exception{
		java.sql.Connection myConn = null;
		java.sql.Statement st = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		User user = null;

		try{ 
		//get connection
			myConn = dataSource.getConnection();
			
		//create sql statement 
			pst = myConn.prepareStatement("SELECT * FROM user WHERE uemail = ? AND upassword = ?");
			pst.setString(1, inputEmail);
			pst.setString(2, inputPassword);
			
		//execute query
			rs = pst.executeQuery();
			
			
		//process result set
			while(rs.next()){
				int id = rs.getInt("uid");
				String firstName = rs.getString("ufirst_name");
				String lastName	 = rs.getString("ulast_name");
				String email = rs.getString("uemail");
				String userType = rs.getString("uuser_type");
				String department = rs.getString("udepartment");
				
				user = new User(id, firstName, lastName, email, userType, department);
				
				
			}
			
		} finally {
			close(myConn, st, rs);
		}
		return user;
		
}
	
	
	public List<User> listUsers() throws Exception{
		List<User> users = new ArrayList<>();
		
		java.sql.Connection myConn = null;
		java.sql.Statement st = null;
		ResultSet rs = null;
		
			try {
				myConn = dataSource.getConnection();
				
				String sql = "select * from user";
				
				st = myConn.createStatement();
				rs = st.executeQuery(sql);
				
				//Process result set
				while(rs.next()){
					int id = rs.getInt("uid");
					String firstName = rs.getString("First_name");
					String lastName	 = rs.getString("Last_name");
					String email = rs.getString("Email");
					String userType = rs.getString("Administrative_Assignment");
					String department = rs.getString("Department");
					
					User tempUser = new User(id, firstName, lastName, email, userType, department);
					
					users.add(tempUser);

				}
				return users;
			} finally {
				close(myConn, st, rs);
			}
	
	}
	
	private void close(Connection myConn, java.sql.Statement st, ResultSet rs) {
		try{
			if(rs != null){
				rs.close();
			}
			if(st != null){
				st.close();
			}
			if(myConn != null){
				myConn.close();
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
}
