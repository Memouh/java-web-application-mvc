package mvc.ts.model.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import com.ts.jsp.FunUtils;

import mvc.ts.Java.Activity;
import mvc.ts.Java.Project;

public class ActivityDBUtil {

	@Resource(name = "jdbc/team-specter")
	private DataSource dataSource;

	public ActivityDBUtil() {
	}

	public ActivityDBUtil(DataSource theDataSource) {
		this.dataSource = theDataSource;
	}

	public void createActivity(String title, String startDate, String endDate, String description,
			String classification, String target_desc) throws Exception {

		Connection myConn = null;
		PreparedStatement pst = null;
		
		String sql = "insert into activity(Title, Start, End, Description, Classification, Target_Audience_Description)"
				+ " values (?, ?, ?, ?, ?, ?)";
		//INSERT INTO `team-specter`.`activity` (`Title`, `Start`, `End`, `Description`, `Classification`, `Target_Audience_Description`) VALUES ('title', '2016-12-09', '2016-12-10', 'desc', 'Senior', 'target_desc');


		try {

			myConn = dataSource.getConnection();
			
			pst = myConn.prepareStatement(sql);
			
			pst.setString(1, title);
			pst.setString(2, FunUtils.makeStringToDate(startDate));
			String endDateValue;

			try {
				endDateValue = FunUtils.makeStringToDate(endDate);
				pst.setString(3, endDateValue);
			} catch (Exception e) {
				endDateValue = "0000-00-00";
				pst.setString(3, endDateValue);
			}

			pst.setString(4, description);
			pst.setString(5, classification);
			pst.setString(6, target_desc);
			
			pst.execute();


		} finally {
			close(myConn, pst, null);
		}

	}

	public void create(String title, String startDate, String endDate, String description, String classification,
			String targetAudienceDescription) throws Exception {
		java.sql.Connection myConn = null;
		java.sql.Statement st = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		// the mySQL Insert Statement
		String queryActivity = "insert into Activity(Title, StartDate, EndDate, Description, Classification, Target_Audience_Description)"
				+ " values (?, ?, ?, ?, ?, ?)";
		// String queryAdministrative = "INSERT INTO `Activity_Administrator`
		// (AdministratorID_ActivityFK, AEID_UserFK, Role)

		try {
			// get connection
			myConn = dataSource.getConnection();

			// Create the mySQL insert prepared statement
			pst = myConn.prepareStatement(queryActivity);
			pst.setString(1, title);
			pst.setString(2, FunUtils.makeStringToDate(startDate));
			String endDateValue;

			try {
				endDateValue = FunUtils.makeStringToDate(endDate);
				pst.setString(3, endDateValue);
			} catch (Exception e) {
				endDateValue = "0000-00-00";
				pst.setString(3, endDateValue);
			}

			pst.setString(4, description);
			pst.setString(5, classification);
			pst.setString(6, targetAudienceDescription);

			// execute query
			pst.executeQuery();
		} finally {
			close(myConn, st, rs);
		}
	}

	public int getActivity(String title, String startDate, String endDate, String description, String classification,
			String targetAudienceDescription) throws Exception {
		int idIni = 5;
		java.sql.Connection myConn = null;
		java.sql.Statement st = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			// get connection
			myConn = dataSource.getConnection();

			// mySQL Retrieve Statement
			String retrieveNewActivity = "SELECT FROM activity WHERE Title = ?, StartDate = ?, EndDate = ?, Description = ?,"
					+ " Classification = ?, Target_Audience_Description = ? values(?, ?, ?, ?, ?, ?)";

			// create mySQL retrieve prepared statement
			pst = myConn.prepareStatement(retrieveNewActivity);
			pst.setString(1, title);
			pst.setString(2, startDate);
			pst.setString(3, endDate);
			pst.setString(4, description);
			pst.setString(5, classification);
			pst.setString(6, targetAudienceDescription);

			// execute query
			rs = pst.executeQuery();
			System.out.println(pst.toString());
			System.out.println(rs.toString());
			while (rs.next()) {
				idIni = rs.getInt("ID");
			}

		} finally {
			close(myConn, st, rs);
		}
		return idIni;
	}

	private void close(Connection myConn, java.sql.Statement st, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
			if (myConn != null) {
				myConn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Activity> getActivities() throws Exception {

		List<Activity> activities = new ArrayList<>();

		Connection myConn = null;
		Statement st = null;
		ResultSet rs = null;

		try {
			myConn = dataSource.getConnection();

			String sql = "select * from activity";

			st = myConn.createStatement();
			rs = st.executeQuery(sql);

			//INSERT INTO `team-specter`.`activity` (`Title`, `Start`, `End`, `Description`, `Classification`, `Target_Audience_Description`) VALUES ('title2', '2016-12-12', '2016-12-15', 'description', 'Senior', 'target_desc');

			while (rs.next()) {
				int id = rs.getInt("ID");
				String Title = rs.getString("Title");
				String Start = rs.getString("Start");
				String End = rs.getString("End");
				String Description = rs.getString("Description");
				String Classification = rs.getString("Classification");
				String Target_Audience_Description = rs.getString("Target_Audience_Description");
				Activity tempActivity = new Activity(id, Title, Start, End, Description, Classification,
						Target_Audience_Description);
				// Activity tempActivity = new Activity(id, Title, Start, End,
				// Description, Target_Audience_Description);

				activities.add(tempActivity);
			}
			return activities;
		} finally {
			close(myConn, st, rs);
		}
	}

	public Activity getActivity(String activityID) throws Exception {

		Activity theActivity = null;

		Connection myConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int theActivityID;

		try {
			theActivityID = Integer.parseInt(activityID);

			myConn = dataSource.getConnection();

			String sql = "select * from activity WHERE ID = ?";

			pst = myConn.prepareStatement(sql);

			pst.setInt(1, theActivityID);

			rs = pst.executeQuery();

			if (rs.next()) {

				String Title = rs.getString("Title");
				String Start = rs.getString("Start");
				String End = rs.getString("End");
				String Description = rs.getString("Description");
				String Classification = rs.getString("Classification");
				String Target_Audience_Description = rs.getString("Target_Audience_Description");

				theActivity = new Activity(theActivityID, Title, Start, End, Description, Classification,
						Target_Audience_Description);
			} else {
				throw new Exception("Could not find that project id: " + theActivityID);
			}

			return theActivity;
		} finally {
			close(myConn, pst, rs);
		}
	}

	public void deleteActivity(String activityID) throws Exception {

		Connection myConn = null;
		PreparedStatement pst = null;
		
		try{
			
			int actID = Integer.parseInt(activityID);
			
			myConn = dataSource.getConnection();
			
			String sql = "DELETE from activity WHERE ID=?";
			
			pst = myConn.prepareStatement(sql);
			
			pst.setInt(1, actID);
			
			pst.execute();
			
		} finally {
			close(myConn, pst, null);			
		}
		
	}

	public void updateActivity(Activity updatedActivity) throws Exception{
		
		Connection myConn = null;
		PreparedStatement pst = null;

		try {

			myConn = dataSource.getConnection();

			String sql = "UPDATE activity "
					+ " set Title=?, Start=?, End=?, Description=?, Classification=?, Target_Audience_Description=? WHERE ID=?";

			pst = myConn.prepareStatement(sql);

			pst.setString(1, updatedActivity.getTitle());
			pst.setString(2, updatedActivity.getStartDate());
			pst.setString(3, updatedActivity.getEndDate());
			pst.setString(4, updatedActivity.getActivityDescription());
			pst.setString(5, updatedActivity.getClassification());
			pst.setString(6, updatedActivity.getAudienceDescription());
			pst.setInt(7, updatedActivity.getId());

			pst.execute();
		} finally {
			close(myConn, pst, null);
		}

	}

}
