package com.ts.jsp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FunUtils {
	
	public static String makeItLower(String data){
		return data.toLowerCase();
	}
	// MM/DD/YYYY
	public static String makeStringToDate(String date){
		StringBuilder builder = new StringBuilder();
		builder.append(date.charAt(6));
		builder.append(date.charAt(7));
		builder.append(date.charAt(8));
		builder.append(date.charAt(9));
		builder.append("-");
		builder.append(date.charAt(0));
		builder.append(date.charAt(1));
		builder.append("-");
		builder.append(date.charAt(3));
		builder.append(date.charAt(4));
		return builder.toString();
		
	}
	
	public static boolean startDateBeforeEndDate(String startDate, String endDate) throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if(endDate==null)
        	return true;
        else{
				Date start = (Date) sdf.parse(startDate);
				Date end = sdf.parse(endDate);
				if(start.before(end))
		            return true;
				else
					return false;	
			}
        }
	
}
